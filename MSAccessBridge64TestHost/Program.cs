﻿using System;
using System.Data;
using System.Linq;
using MSAccessBridgeClient;

namespace MSAccessBridge64TestHost
{
	class Program
	{
		private const string testConnect = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=motion.mdb;";
		
		private static readonly IMSAccessBridge64Client bridgeClient = MSAccessBridge64Client.Instance;

		static void Main(string[] args)
		{
			SynchronousExample();
		}

		private static void SynchronousExample()
		{
			try
			{
				DataTable table = bridgeClient.Read(testConnect);
				PrintDataTable(table);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Console.Read();
			}
		}

		private static void KeepAliveExample()
		{
			var client = MSAccessBridge64Client.Instance;
			// set interval for sending messaging from wcf client to 3 minutes (default is 5 minutes)
			client.KeepAliveInterval = TimeSpan.FromSeconds(30);
			// set additional delay to invoke keep-alive on wcf server (default is 100 milliseconds)
			// this means that on wcf server shutdown timeout will be set to 3 minuites 1 seconds, while wcf client will be sending 
			// keep alive message each 3 minutes exactley. 5 seconds is to account for wcf call delay
			client.DelayInterval = TimeSpan.FromSeconds(1);
			// start timer to sends keep-alive message (default is false)
			client.SendKeepAliveMessage = true;
			client.Read(testConnect);
			Console.Read();
		}

		private static void AsynchronousExample()
		{
			try
			{
				bridgeClient.ReadAsync(testConnect);
				bridgeClient.ReadAsyncCompleted += OnReadAsyncCompleted;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Console.Read();
				return;
			}
			Console.Read();
		}

		static void OnReadAsyncCompleted(object sender, ReadOperationEventArgs e)
		{
			if (e.IsValid)
			{
				PrintDataTable(e.Table);	
			}
			else
			{
				Console.WriteLine(e.Exception);
				Console.Read();
			}
			
		}

		private static void PrintDataTable(DataTable table)
		{
			foreach (DataRow row in table.Rows)
			{
				string s = table.Columns.Cast<DataColumn>().
					Aggregate(string.Empty, (current, column) => current + (row[column].ToString() + " "));

				Console.WriteLine(s);
			}
			Console.Read();
			bridgeClient.CloseHost();  // always close client
		}
	}
}
