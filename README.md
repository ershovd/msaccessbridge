### Intro ###
This repo contains source code for Odesk project [Develop DCOM Object for 64-to-32 bit Bridge](https://www.odesk.com/jobs/~013b97ae3051847c68)

### Overview ###
The project is implemented as WCF service with 32-bit server (MSAccessBridgeWCF WCF assembly and MSAccessBridge32Host server console host) and client (MSAccessBridgeClient  main client library and MSAccessBridge64TestHost  example console app) sides. There are 2 solution files: MSAccessBridgeServer.sln  for server and MSAccessBridge.sln for client

### Test example ###

* Locate release\ folder Make sure file release\LcfSample.mdb exists.
* Build all 4 projects with release configuration
* Start test console client at release\MSAccessBridge64TestHost.exe. It will start the server process MSAccessBridge32Host automatically and should produce output from MS Access database

### How to use ###

You need only reference MSAccessBridgeClient assembly (assuming server is up and running). Main functionality class is MSAccessBridge64Client. It provides methods for synchronous and  asynchronous reading (server methods are all asynchronous). For usage example you can see MSAccessBridge64TestHost\Program.cs

You can read synchronously like that:

```
#!c#

IMSAccessBridge64Client client = MSAccessBridge64Client.Instance;
				DataTable table = client.Read("connection-string");
client.CloseHost();  // close server process host
```
Reading asynchronously is just a little trickier: you have to subscribe to event ReadAsyncCompleted
and handles resulting DataTable or any error occurred in the event handler, like that


```
#!c#

				IMSAccessBridge64Client client = MSAccessBridge64Client.Instance;
				client.ReadAsync(testConnect);
				client.ReadAsyncCompleted += OnReadAsyncCompleted;
client.CloseHost();  // close server process host
```

```
#!c#
 void OnReadAsyncCompleted(object sender, ReadOperationEventArgs e)
		{
			if (e.IsValid)
			{
				var table = e.Table;  // do something with data
			}
			else
			{
				throw e.Exception;  // handles exception
			}			
		}
```

### Important implementation details ###
* To ensure that MSAccessBridge32Host (Wcf server) runs without admin privileges default Mex Http endpoint (and associated binding, base address and Service behavior) should be disabled. This is done in MSAccessBridgeWCF\App.config file (see commented sections). This also prevents Wcf Service to be discover by new clients. In short, to work through http wcf server has to have admin permission to access specific port http port. By removing mexHttpBiding and working through different transport protocol (named pipes) we overcome this limitation. See more http://wcftutorial.net/metadata-exchange-endpoint.aspx
* MSAccessBridge64Client is implemented as a thread-safe Singleton.You can get singleton instance using MSAccessBridge64Client.Instance. This is to ensure that only 1 Wcf client will be run at time.More about singleton implementation http://csharpindepth.com/Articles/General/Singleton.aspx
* The MSAccessBridge64Client (Wcf client) starts MSAccessBridge32Host (Wcf server) process automatically.You can also start the server process manually be calling StartHost() method.
* The MSAccessBridge32Host  can run only 1 server process instance at time. This is impemented using mutex. More about it http://sanity-free.org/143/csharp_dotnet_single_instance_application.html  This is to ensure that potential multiple Wcf clients (from multiple instance of 64-bit host application) work with only 1 Wcf server
* The client provide CloseHost() method for closing Wcf server and finishing wcf server process. Please note that CloseHost() do not call automatically, you have to call it explicitly when you done working with client.
* Wcf server has timeout property, after which it terminates the server process. Default timeout is 5 minutes (can be set in MSAccessBridge32Host\Program.cs). You can also use KeepAlive() method to send keep-alive message explicitly. The client can send a keep-alive message automatically to wcf server to prevent it from shutting down. This can be done by like that:

```
#!c#
			var client = MSAccessBridge64Client.Instance;
			// set interval for sending messaging from wcf client to 3 minutes (default is 5 minutes)
			client.KeepAliveInterval = TimeSpan.FromMinutes(3);
			// set additional delay to invoke keep-alive on wcf server (default is 100 milliseconds)
			// this means that on wcf server shutdown timeout will be set to 3 minuites 1 seconds, while wcf client will be sending 
			// keep alive message each 3 minutes exactly. 5 seconds is to account for wcf call delay
			client.DelayInterval = TimeSpan.FromSeconds(1);
			// start timer to sends keep-alive message (default is false)
			client.SendKeepAliveMessage = true;
```

* When starting wcf server process, wcf client creates or open existing EventWaitHandle "Global\MSAccessBridge32Host.Started" to get to know when the wcf server is started. This EventWaitHandle is signaled from Wcf server process when it's ready. Please note that if "Global\MSAccessBridge32Host.Started" is not signaled (for example name is altered or Wcf server process aborted before fully starting wcf server) the client can wait forever.
* Please note that Read, ReadAsyncCompleted  all can throw exceptions. If server-side exception occurred it will be logged on serve and transmitted to client.
Also, if exception is thrown in the end of asynchronous reading operation, then you can access Exception property of ReadOperationEventArgs in event handler for asynchronous reading ReadAsyncCompleted.
* Specific exception types such as MatchWorks.FormatException and MatchWorks.ConnectionException cann't be transmitted thru WCF (because they are marked as [Serializable]. Instead they will be recreated with the same message property at server side and then at client side. So in you 64-bit main app you simply have to use ConnectionException and FormatException defined in assembly MSAccessBridgeClient  
* There is configuration settings for both client and server maximum amount of data transferred (maxReceivedMessageSize). Right now it is set to 10485760bytes (10 Mb), but it can be changed in release\server\MSAccessBridgeWCF.dll.config  and release\client\MSAccessBridge64TestHost.exe.config  or in App.Config in respective projects (when building).
* When serializing datatable from access WCF library set its tablename to "MSAccessBridge", if the one is empty. This is necessary to prevent serialization errors.
* Currently server-side designed to run on the same machine using netNamedPiped protocol without need for administrative privileges. See more on why choosing Name pipe http://wcftutorial.net/wcf-types-of-binding.aspx