﻿using System;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using MSAccessBridgeWCF;

namespace MSAccessBridge32Host
{
	static class Program
	{
		// this is for signle running instance only http://sanity-free.org/143/csharp_dotnet_single_instance_application.html
		static Mutex mutex = new Mutex(true, "721F6A00-0066-4FCB-8119-EB96ACB97ABF");

		private static readonly TimeSpan defaultWcfServerKeepAlive = TimeSpan.FromMinutes(5);
		private static System.Timers.Timer timer;
		
		private static ServiceHost host;

		static void Main(string[] args)
		{
			if (mutex.WaitOne(TimeSpan.Zero, true))
			{
				RunWcfHost();
				mutex.ReleaseMutex();
			}
			else
			{
				Console.WriteLine("Only 1 instance of MSAccessBridge32Host allowed to run.");
			}
		}

		private static void RunWcfHost()
		{
			try
			{
				InitHostTimer();

				// Name piped choosed for working best on same machine http://wcftutorial.net/wcf-types-of-binding.aspx
				var netNameAddress = new Uri("net.pipe://localhost");

				IMSAccessBridgeService serviceClient = new MSAccessBridge();
				serviceClient.KeepAliveOccured += ServiceClientKeepAliveOccured;


				// This is for MEX service and generally better be disabled in production
				// In short, it used to dicover WCF services by new clients
				var mexBasAddress = new Uri("http://localhost:8732/accessbridge");
				var arr = new[] { netNameAddress }; //, mexBasAddress};
				host = new ServiceHost(serviceClient, arr);
				host.Closed += OnClosed;

				// uncomment to use Mex, comment out during release
				//AddServiceMetadaBehaviour(host);

				host.Open();

				SignalWaitEvenHandle();

				Console.WriteLine("The service is ready at {0}", netNameAddress);
				Console.WriteLine("Press <Enter> to stop the service.");
				Console.ReadLine();

				host.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}

		private static void InitHostTimer()
		{
			var milliseconds = defaultWcfServerKeepAlive.TotalMilliseconds;
			timer = new System.Timers.Timer(milliseconds)
				        {
					        AutoReset = true
				        };
			timer.Elapsed += OnTimerElapsed;
			timer.Start();
		}

		static void OnTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			host.Close();
		}

		static void ServiceClientKeepAliveOccured(object sender, KeepAliveEventArgs e)
		{
			TimeSpan timeSpan = e.Interval;
			if (timeSpan.TotalMilliseconds > 0)
			{
				timer.Interval = timeSpan.TotalMilliseconds;	
			}
		}

		#region EventWaitHandle signaling

		private const string serverStartedWaitEventHandle = @"Global\MSAccessBridge32Host.Started";

		/// <summary>
		/// Create security settings for event wait handle
		/// </summary>
		private static EventWaitHandleSecurity CreateEventHandleSecurity()
		{
			var users = new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null);
			var rule = new EventWaitHandleAccessRule(users, EventWaitHandleRights.Synchronize | EventWaitHandleRights.Modify, AccessControlType.Allow);
			var security = new EventWaitHandleSecurity();
			security.AddAccessRule(rule);
			return security;
		}

		/// <summary>
		/// Open or create EventWaitHandle and signal that WCF service is runinning
		/// </summary>
		private static void SignalWaitEvenHandle()
		{
			EventWaitHandle wh;
			try
			{
				wh = EventWaitHandle.OpenExisting(serverStartedWaitEventHandle, EventWaitHandleRights.Synchronize | EventWaitHandleRights.Modify);
			}
			catch (WaitHandleCannotBeOpenedException)
			{
				// this is if we start MSAccessBridge32Host manually
				wh = CreateWaitEventHandle();
			}

			wh.Set();
		}

		private static EventWaitHandle CreateWaitEventHandle()
		{
			var security = CreateEventHandleSecurity();
			bool createdNew;
			return new EventWaitHandle(false, EventResetMode.ManualReset, serverStartedWaitEventHandle, out createdNew, security);
		}

		#endregion

		private static void OnClosed(object sender, EventArgs e)
		{
			Environment.Exit(0);
		}

		private static void AddServiceMetadaBehaviour(ServiceHostBase myHost)
		{
			var smb = new ServiceMetadataBehavior();
			smb.HttpGetEnabled = true;
			smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
			myHost.Description.Behaviors.Add(smb);
		}
	}
}
