﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ServiceModel;
using System.Threading;
using System.Timers;


namespace MSAccessBridgeClient
{
	/// <summary>
	/// Client class for calling WCF service method for reading data from 32-bit MS Access
	/// </summary>
	[Serializable]
	public class MSAccessBridge64Client : IMSAccessBridge64Client
	{
		#region Singleton
		// Reason for such singleton implementation
		// http://csharpindepth.com/Articles/General/Singleton.aspx

		private static readonly IMSAccessBridge64Client instance = new MSAccessBridge64Client();

		// Explicit static constructor to tell C# compiler
		// not to mark type as beforefieldinit
		static MSAccessBridge64Client()
		{
		}

		private MSAccessBridge64Client()
		{
			serviceClient = new MSAccessBridgeServiceClient();
			DelayInterval = TimeSpan.FromMilliseconds(100); // default delay interval for alloing Wcf server process to run
			KeepAliveInterval = TimeSpan.FromMinutes(5);  // default interval for sending keep-alive message from client
			timer.Elapsed += (sender, e) => KeepAlive();
		}


		public static IMSAccessBridge64Client Instance
		{
			get
			{
				return instance;
			}
		}

		#endregion

		private MSAccessBridgeServiceClient serviceClient;
		private readonly System.Timers.Timer timer = new System.Timers.Timer(); // initilize here to prevent NullReference
		private TimeSpan keepAliveInterval;
		private TimeSpan delayInterval;

		public DataTable Read(string connection)
		{
			if (string.IsNullOrEmpty(connection)) throw new ArgumentNullException("connection");

			// this try\cacth stuff for solving problem with using {}
			// In short: by default using statemnet can throw on compeletion, which is hard to catch and inconvinient
			// Now, it should throw only on client method calls
			// see https://msdn.microsoft.com/en-us/library/aa355056.aspx
			
			try
			{
				PrepareWcfCall();
				var data = serviceClient.ReadAsync(connection);
				
				if (serviceClient.State != CommunicationState.Faulted)
				{
					serviceClient.Close();
				}
				return data;
			}
			catch (Exception e)
			{
				serviceClient.Abort();
				MatchAndThrowException(e);
			}
			finally
			{
				if (serviceClient.State != CommunicationState.Closed)
				{
					serviceClient.Abort();
				}
			}

			//this is for compilation only and should never be reached.
			// MatchAndThrowException(e); guruantee to throw something exception
			return null;
		}

		public void ReadAsync(string connection)
		{
			if (string.IsNullOrEmpty(connection)) throw new ArgumentNullException("connection");
			
			try
			{
				PrepareWcfCall();
				// TODO: think about modified closure for client http://confluence.jetbrains.com/display/ReSharper/Access+to+modified+closure
				serviceClient.BeginReadAsync(connection, result =>
					                                       {
						                                       try
						                                       {
							                                       // handles when EndReadAsync(result) throws something
							                                       DataTable table = serviceClient.EndReadAsync(result);
							                                       OnReadDataTableCompleted(new ReadOperationEventArgs(table, null));
							                                       if (serviceClient.State != CommunicationState.Faulted)
							                                       {
								                                       serviceClient.Close();
							                                       }
						                                       }
						                                       catch(Exception ex)
						                                       {
							                                       serviceClient.Abort();
																   OnReadDataTableCompleted(new ReadOperationEventArgs(null, ex));
						                                       }
					                                       }, null);

			}
			catch (Exception e) 
			{
				serviceClient.Abort();
				MatchAndThrowException(e);
			}
		}
		/// <summary>
		/// Match exception with server-side and throw corresponding cient exception
		/// </summary>
		/// <param name="ex"></param>
		private static void MatchAndThrowException(Exception ex)
		{
			if (ex != null)
			{
				var formatEx = ex as FaultException<MSAccessBridgeWCF.FormatException>;
				if (formatEx != null)
				{
					throw new FormatException(formatEx.Detail.Message);
				}

				var connectEx = ex as FaultException<MSAccessBridgeWCF.ConnectionException>;
				if (connectEx != null)
				{
					throw new ConnectionException(connectEx.Detail.Message);
				}

				var faulEx = ex as FaultException;  // this is for catched exceptions on server
				if (faulEx != null)
				{
					throw new InvalidOperationException(faulEx.Message);
				}

				throw new InvalidOperationException(ex.Message); // this case for uncatched exception on server (CommunicationException, time out etc)
			}

			throw new InvalidOperationException("Exception is null");
		}

		public event EventHandler<ReadOperationEventArgs> ReadAsyncCompleted;

		public void CloseHost()
		{
			if (!IsWcfProcessRunning) return;

			try
			{
				PrepareWcfCall();
				serviceClient.CloseHost();
			}
			catch (Exception e)
			{
				serviceClient.Abort();
			}
		}

		public void StartHost()
		{
			if (IsWcfProcessRunning) return;

			StartWcfServerProcess();
		}

		#region Keep Alive support

		/// <summary>
		/// Indicates whether to send keep-alive to Wcf Service automatically
		/// </summary>
		public bool SendKeepAliveMessage
		{
			get { return timer.Enabled; }
			set { timer.Enabled = value; }
		}
		
		/// <summary>
		/// Specifies interval for keeping Wcf server alive 
		/// </summary>
		public TimeSpan KeepAliveInterval
		{
			get { return keepAliveInterval; }
			set
			{
				if (value.TotalMilliseconds <= 0) 
					throw new ArgumentOutOfRangeException("KeepAliveInterval should be positive!");

				keepAliveInterval = value;
				timer.Interval = keepAliveInterval.TotalMilliseconds;
			}
		}

		/// <summary>
		/// Specifies the additional interval for allowing Wcf server to keep-alive
		/// </summary>
		/// <remarks>Total Wcf keep-alive interval = KeepAliveInterval + DelayInterval </remarks>
		public TimeSpan DelayInterval
		{
			get { return delayInterval; }
			set
			{
				if (value.TotalMilliseconds <= 0)
					throw new ArgumentOutOfRangeException("DelayInterval should be positive.");
				delayInterval = value;
			}
		}

		/// <summary>
		/// Manually call keep-alive message on server
		/// </summary>
		public void KeepAlive()
		{
			// this is total interval in Wcf server that will keep the server alive
			TimeSpan interval = KeepAliveInterval + DelayInterval;
			KeepAlive(interval);
		}

		private void KeepAlive(TimeSpan interval)
		{
			try
			{
				PrepareWcfCall();
				serviceClient.KeepAlive(interval.TotalMilliseconds);
			}
			catch (Exception)
			{
				serviceClient.Abort();
			}
		}

		#endregion

		#region EventWaitHandle signaling

		/// <summary>
		/// Start WCF service process and wait for it inintitalize
		/// </summary>
		private static void StartWcfServerProcess()
		{
			var wh = CreateWaitEventHandle();
			string fileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + Path.DirectorySeparatorChar + WcfHostProcessName + ExeExtension;
			ProcessStartInfo info = new ProcessStartInfo();
			info.FileName = fileName;
			info.CreateNoWindow = false;
			info.UseShellExecute = false;
			
			var process = new Process {StartInfo = info};
			process.Start();

			// WARNING: this will block current thread until Global\MSAccessBridge32Host.Started signal received
			wh.WaitOne();
		}

		/// <summary>
		/// Create wait event handle with specific security settings
		/// </summary>
		/// <returns></returns>
		private static EventWaitHandle CreateWaitEventHandle()
		{
			var security = CreateEventHandleSecurity();
			bool created;
			return new EventWaitHandle(false, EventResetMode.ManualReset, serverStartedWaitEventHandle, out created, security);
		}

		/// <summary>
		/// Create security settings for event-wait handle
		/// </summary>
		private static EventWaitHandleSecurity CreateEventHandleSecurity()
		{
			var users = new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null);
			var rule = new EventWaitHandleAccessRule(users, EventWaitHandleRights.Synchronize | EventWaitHandleRights.Modify,AccessControlType.Allow);
			var security = new EventWaitHandleSecurity();
			security.AddAccessRule(rule);
			return security;
		}

		/// <summary>
		/// Returns true is process with name MSAccessBridge32Host is running
		/// </summary>
		private bool IsWcfProcessRunning
		{
			get
			{
				var processes = Process.GetProcessesByName(WcfHostProcessName);
				return processes.Length > 0;
			}
		}

		private const string serverStartedWaitEventHandle = @"Global\MSAccessBridge32Host.Started";
		private const string WcfHostProcessName = "MSAccessBridge32Host";
		private const string ExeExtension = ".exe";
		#endregion

		/// <summary>
		/// Check if WCF service is running and refresh service client
		/// </summary>
		private void PrepareWcfCall()
		{
			if (!IsWcfProcessRunning)
			{
				StartWcfServerProcess();
			}

			ServiceHelper<MSAccessBridgeServiceClient>.FixToOpen(ref serviceClient);
		}

		private void OnReadDataTableCompleted(ReadOperationEventArgs e)
		{
			EventHandler<ReadOperationEventArgs> handler = ReadAsyncCompleted;
			if (handler != null) 
				handler(this, e);
		}
	
	}
}
