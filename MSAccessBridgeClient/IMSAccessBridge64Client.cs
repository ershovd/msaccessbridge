using System;
using System.Data;

namespace MSAccessBridgeClient
{
	/// <summary>
	/// Client 64-bit interface to Read data from MS Access Database
	/// </summary>
	public interface IMSAccessBridge64Client
	{
		/// <summary>
		/// Read data from MS Access database, synchronous version
		/// </summary>
		/// <param name="connection">Connection string to connect to Access</param>
		/// <returns>Datatable with query results</returns>
		/// <exception cref="ArgumentNullException">If connection is null or empty</exception>
		/// <exception cref="FormatException">If MatchWorks.FormatException is thrown</exception>
		/// <exception cref="ConnectionException">If MatchWorks.ConnectionException is thrown</exception>
		/// <exception cref="InvalidOperationException">If other exception on wcf server occured</exception>
		DataTable Read(string connection);

		/// <summary>
		/// Read data from MS Access database, asynchronous version
		/// </summary>
		/// <param name="connection">Connection string to connect to Access</param>
		/// <exception cref="ArgumentNullException">If connection is null or empty</exception>
		/// <exception cref="FormatException">If MatchWorks.FormatException is thrown</exception>
		/// <exception cref="ConnectionException">If MatchWorks.ConnectionException is thrown</exception>
		/// <exception cref="InvalidOperationException">If other exception on wcf server occured</exception>
		void ReadAsync(string connection);
		
		/// <summary>
		/// Event fired when async read is complete, provides Datatable and exception instance, if occured
		/// </summary>
		event EventHandler<ReadOperationEventArgs> ReadAsyncCompleted;

		/// <summary>
		/// Send signal to WCF Service to close server process
		/// </summary>
		void CloseHost();

		/// <summary>
		/// Start the WCF server explictly
		/// </summary>
		void StartHost();

		/// <summary>
		/// Indicates whether to send keep-alive to Wcf Service automatically
		/// </summary>
		bool SendKeepAliveMessage { get; set; }

		/// <summary>
		/// Specifies interval for sending keep-alive message from client
		/// </summary>
		/// <exception cref="ArgumentOutOfRangeException">If KeepAliveInterval is not positive</exception>
		TimeSpan KeepAliveInterval { get; set; }

		/// <summary>
		/// Specifies the additional interval for allowing Wcf server to keep-alive
		/// </summary>
		/// <exception cref="ArgumentOutOfRangeException">If DelayInterval is not positive</exception>
		/// <remarks>Total Wcf keep-alive interval = KeepAliveInterval + DelayInterval </remarks>
		TimeSpan DelayInterval { get; set; }

		/// <summary>
		/// Manually call keep-alive message on server
		/// </summary>
		void KeepAlive();
	}
}