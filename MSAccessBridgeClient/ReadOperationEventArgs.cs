using System;
using System.Data;

namespace MSAccessBridgeClient
{
	/// <summary>
	/// Event arguments for results of async operation
	/// </summary>
	[Serializable]
	public class ReadOperationEventArgs : EventArgs
	{
		public ReadOperationEventArgs(DataTable table, Exception exception)
		{
			Table = table;
			Exception = exception;
		}
		/// <summary>
		/// Datatable obtained from server
		/// </summary>
		public DataTable Table { get; private set; }
		/// <summary>
		/// Exception info if error occured during async reading
		/// </summary>
		public Exception Exception { get; private set; }

		/// <summary>
		/// True if Table contains data and no exception occured
		/// </summary>
		public bool IsValid
		{
			get { return Exception == null && Table != null; }
		}
	}
}