using System;
using System.ServiceModel;

namespace MSAccessBridgeClient
{
	/// <summary>
	/// Helper class to fix ServiceHost instance into valid state
	/// </summary>
	/// <typeparam name="TCommunication"></typeparam>
	public static class ServiceHelper<TCommunication> where TCommunication : class, ICommunicationObject, new()
	{
		/// <summary>
		/// Fix communication object into Open state
		/// </summary>
		public static void FixToOpen(ref TCommunication co)
		{
			FixToOpen(ref co, () => new TCommunication());
		}

		/// <summary>
		/// Fix communication object into Open state
		/// </summary>
		public static void FixToOpen(ref TCommunication co, Func<TCommunication> createMethod)
		{
			if ((co != null) && (co.State == CommunicationState.Faulted))
			{
				co.Abort();
				co = null;
			}
			if (co == null || co.State == CommunicationState.Closed)
			{
				co = createMethod();
			}
		}
	}
}