using System;
using System.Runtime.Serialization;

namespace MSAccessBridgeClient
{
	/// <summary>
	/// Client-side for MatchWorks.FormatException
	/// </summary>
	[Serializable]
	public class FormatException : Exception
	{
		public FormatException()
		{
		}

		public FormatException(string message) : base(message)
		{
		}

		public FormatException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected FormatException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}