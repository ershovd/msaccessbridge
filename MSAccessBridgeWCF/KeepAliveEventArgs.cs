using System;

namespace MSAccessBridgeWCF
{
	[Serializable]
	public class KeepAliveEventArgs : EventArgs
	{
		public KeepAliveEventArgs(TimeSpan interval)
		{
			Interval = interval;
		}
		/// <summary>
		/// Interval indicatiting how long to keep WCF host alive
		/// </summary>
		public TimeSpan Interval { get; private set; }
	}
}