﻿using System;
using System.Data;
using System.ServiceModel;

namespace MSAccessBridgeWCF
{
	/// <summary>
	/// Bridge (32-to-64) service interface to read data from MS Access
	/// </summary>
	[ServiceContract(Namespace = "http://spartansw.com", ConfigurationName = "MSAccessBridgeWCF.MSAccessBridge")]
	public interface IMSAccessBridgeService
	{
		/// <summary>
		/// Begin Read data from MS Access and return the Datatable, asynchronous version
		/// </summary>
		/// <param name="connection">Connection string to MS Access</param>
		/// <param name="callback">Callback method to async operation</param>
		/// <param name="state"></param>
		/// <returns>Result of asynchronous call</returns>
		[FaultContract(typeof(FormatException))]
		[FaultContract(typeof(ConnectionException))]
		[FaultContract(typeof(string))]
		[OperationContract(AsyncPattern = true)]
		IAsyncResult BeginReadAsync(string connection, AsyncCallback callback, object state);

		/// <summary>
		/// End Read data from MS Access and return the Datatable, asynchronous version
		/// </summary>
		/// <param name="result">Result of the asynchronous operation</param>
		/// <returns>DataTable with results</returns>
		[FaultContract(typeof(FormatException))]
		[FaultContract(typeof(ConnectionException))]
		[FaultContract(typeof(string))]
		DataTable EndReadAsync(IAsyncResult result);

		/// <summary>
		/// Send a "close host" message to WCF service Host
		/// </summary>
		[OperationContract(IsOneWay = true)]
		void CloseHost();

		/// <summary>
		/// Indicates that wcf service should be kept running for specified milliseconds interval
		/// </summary>
		[OperationContract]
		void KeepAlive(double interval);

		/// <summary>
		/// Event for service host base occuring when keep-alive message has arrived
		/// </summary>
		event EventHandler<KeepAliveEventArgs> KeepAliveOccured;
	}
}
