﻿using System;
using System.Data;
using System.ServiceModel;
using System.Threading.Tasks;
using MatchWorks = MatchWorksWpf.Services.FileImport;

namespace MSAccessBridgeWCF
{
	/// <summary>
	/// Class implemnting Read service from MSAccess
	/// </summary>
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	public class MSAccessBridge : IMSAccessBridgeService
	{
		/// <summary>
		/// Begin Read data from MS Access and return the Datatable, asynchronous version
		/// </summary>
		/// <param name="connection">Connection string to MS Access</param>
		/// <param name="callback">Callback method to async operation</param>
		/// <param name="state"></param>
		/// <returns>Result of asynchronous call</returns>
		public IAsyncResult BeginReadAsync(string connection, AsyncCallback callback, object state)
		{
			
			Task<DataTable> task = ProcessWithExceptions(() => StartReadTask(connection, state));
			
			var result = task.ContinueWith(res =>
				                         {
					                         HandlingAbnormalCondition(res);
											 callback(res);
				                         });

			return result;
		}

		/// <summary>
		/// Handles canceled or faulted condition of async task on server
		/// </summary>
		/// <param name="res">Task with not IsCompleted condition</param>
		private void HandlingAbnormalCondition(Task res)
		{
			if (res.IsCanceled) // this generally should not happens
			{
				// TODO: think if we need to do something when cancelled
				Console.WriteLine("Read task id:{0} was canceled", res.Id);
			}
			else if (res.IsFaulted) // this is when exception thrown while reading
			{
				LogInnerTaskException(res);
			}
		}
		
		/// <summary>
		/// Log to server
		/// </summary>
		/// <param name="res"></param>
		private void LogInnerTaskException(Task res)
		{
			Exception ex = res.Exception;
			if (ex != null)
			{
				ex = GetFromAggregateException(ex);

				Console.WriteLine("Task id: {0} failed: {1}", res.Id, ex);
			}
		}

		private static Exception GetFromAggregateException(Exception ex)
		{
			while (ex is AggregateException && ex.InnerException != null)
			{
				ex = ex.InnerException;
			}
			return ex;
		}

		/// <summary>
		/// End Read data from MS Access and return the Datatable, asynchronous version
		/// </summary>
		/// <param name="result">Result of the asynchronous operation</param>
		/// <returns>DataTable with results</returns>
		public DataTable EndReadAsync(IAsyncResult result)
		{
			var res = result as Task<DataTable>;
			if (res == null)
			{
				ThrowFaultExeception("Returned task is null");
			}
			int id = res.Id;
			if (res.IsCanceled)
			{
				ThrowFaultExeception(string.Format("Task id {0} is canceled.", id));
			}
			if (res.IsFaulted)
			{
				if (res.Exception != null)
				{
					Exception ex = GetFromAggregateException(res.Exception);
					
					// Just a convience method for throwing appropriate exception
					ProcessWithExceptions<DataTable>(() =>
						                                 {
							                                 throw ex;
						                                 });
				}
				else
				{
					ThrowFaultExeception(string.Format("Task {0} faulted with no exception", id));
				}
			}

			if (res.Result == null)
			{
				ThrowFaultExeception("Task result is null");
			}
			return res.Result;
		}

		public void CloseHost()
		{
			OperationContext.Current.Host.Abort();
		}

		public void KeepAlive(double interval)
		{
			OnKeepAliveOccured(new KeepAliveEventArgs(TimeSpan.FromMilliseconds(interval)));
		}

		public event EventHandler<KeepAliveEventArgs> KeepAliveOccured;

		private void OnKeepAliveOccured(KeepAliveEventArgs e)
		{
			EventHandler<KeepAliveEventArgs> handler = KeepAliveOccured;
			if (handler != null) 
				handler(this, e);
		}

		/// <summary>
		/// Process some method with appropriate exception handling
		/// </summary>
		/// <typeparam name="TType">Type of method to be processed</typeparam>
		/// <param name="someFunc">Function of returned type be processed</param>
		/// <returns>Instance of processed type</returns>
		private static TType ProcessWithExceptions<TType>(Func<TType> someFunc)
		{
			try
			{
				return someFunc.Invoke();
			}
			catch (MatchWorks.FormatException e)
			{
				var exception = new FormatException(e.Message); // make sure to include only stuff that is serializable
				ThrowFaultExeception(exception);
			}
			catch (MatchWorks.ConnectionException e)
			{
				var exception = new ConnectionException(e.Message); // make sure to include only stuff that is serializable
				ThrowFaultExeception(exception);
			}
			catch (Exception e)
			{
				ThrowFaultExeception(e.Message);
			}
			throw new InvalidOperationException("Uncatched exception in Read!"); // this code should be unreachable
		}

		/// <summary>
		/// Throw FaultException wrapped with exception and reason
		/// </summary>
		/// <typeparam name="TException">Type of the exception info to be thrown</typeparam>
		/// <param name="exception">Instance of exception info</param>
		/// <remarks>TException must be marked as [Serialazable], otherwise exception will not be transmitted to client</remarks>
		private static void ThrowFaultExeception<TException>(TException exception) where TException : Exception
		{
			throw new FaultException<TException>(exception, exception.Message);
		}

		/// <summary>
		/// Throw FaultException wrapped with string desscription and reason
		/// </summary>
		/// <param name="exception">Exception info</param>
		private static void ThrowFaultExeception(string exception) 
		{
			throw new FaultException<string>(exception, exception);
		}
	
		/// <summary>
		/// Start Task asynchronously for InternalRead method
		/// </summary>
		/// <param name="connection">Connection string to connect to MS Access</param>
		/// <param name="state">Asyncronous state</param>
		private static Task<DataTable> StartReadTask(string connection, object state)
		{
			return Task<DataTable>.Factory.StartNew(r => InternalRead(connection), state);
		}
		
		/// <summary>
		/// Reads from MS Access Database using MatchWorks.APLCFReader
		/// </summary>
		/// <param name="connection">Connection string to connect to MS Access</param>
		/// <returns>DataTable populated with returned rows</returns>
		/// <remarks>If TableName is empty set it to MSAccessBridge</remarks>
		private static DataTable InternalRead(string connection)
		{
			var reader = MatchWorks.APLCFReader.CreateReader();
			DataTable table = reader.Read(connection);
			if (string.IsNullOrEmpty(table.TableName))
			{
				table.TableName = "MSAccessBridge";
			}
			return table;
		}
	}
}